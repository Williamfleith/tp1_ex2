# -*- coding: utf-8 -*-
"""
Created on 25/03/2020
by William FLEITH
"""



class SimpleCalculator:
    """
    this the class simple calculator to calculate sum substract multiply and divide
    """

    def __init__(self, num1, num2):
        """
        this is the initiation function
        """
        self.number1 = num1
        self.number2 = num2

    def sum(self):
        """
        this is the sum function
        """
        print(self.number1, "+", self.number2, "=")
        result = self.number1 + self.number2
        return result

    def substract(self):
        """
        this is the substract function
        """
        print(self.number1, "-", self.number2, "=")
        result = self.number1 - self.number2
        return result

    def multiply(self):
        """
        this is the multiply function
        """
        print(self.number1, "*", self.number2, "=")
        result = self.number1 * self.number2
        return result

    def divide(self):
        """
        this is the divide function
        """
        print(self.number1, "/", self.number2, "=")
        result = self.number1 / self.number2
        return result

#main code
RES = SimpleCalculator(2, 1)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())
