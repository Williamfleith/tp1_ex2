README ex2.py :

created on 25/03/2020 by William FLEITH
Rated 10/10 on pylint. 

When you run the programm, it will do it with the value 2 and 1. 
You can change it on the file ex2.py line 54. 

Ex3 :
after using Black and pylint command, it shows different points :
- remove useless spaces and lines
- comment at the right place and indentatiob(just after the creation of a class or function)
- make a space before and after characters such as " = ", " < ", " + "..
- double point don't need space before ex : class SimpleCalculator:
- don't forget the "# -*- coding: utf-8 -*-" line at the beginning of the file.
- don't forget the first comment with creation date, creator name, ...
- Becareful of how to write variables according to the PEP8 standards.